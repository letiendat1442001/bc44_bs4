/*
Mảng trong Js - Array


1. Cách tạo 
    - cách tạo
    - Sử dụng cách nào? tại sao? 
    - Kiểm tra data type?

2. Truy xuất mảng
    - Độ dài mảng
    - Lấy phần tử theo index 

*/

/*làm việc với mảng */
var languages = [
    'Javascript',
    'PHP',
    'Ruby'
];

// 1. to string (chuyển arr sang string)

// console.log(languages.toString());

// 2. join (Hàm join sẽ nối các phần tử của mảng thành một chuỗi, các phần tử được ngăn cách nhau bởi kí tự do người dùng quy định)

console.log(languages.join(''));

// 3. pop (xóa đi phần tử ở cuối mảng và trả lại phần tử đã xóa)

// console.log(languages.pop());


// 4. push (thêm 1 hoặc nhiều phần tử ở cuối arr)

console.log(languages.push('dart', 'java'));


// 5. shift(xóa đi phẩn từ đầu mảng và trả lại phần tử đó)

// console.log(languages.shift());



// 6. unshift (thêm 1 hoặc nhiều phần tử vào đầu mảng và trả về độ dài mới của mảng)

// console.log(languages.unshift('dart2'));




// 7. splicing (xóa, cắt, chèn phần tử mới vào mảng)

languages.splice(1, 1, 'dart3')




// 8. concat (nối arr)

var languages2 = [
    'Dart',
    'Java'
]

console.log(languages.concat(languages2));

// 9. slicing (cắt một vài element của arr)

console.log(languages.slice(1));

console.log(languages);










