//khai báo biến dùng VAR (variable) và có phân biệt hoa thường 
// var fullName = 'letiendat';
// alert(fullName);

/**
 * một số hàm Built-in
    1. Alert (hiện ra thông báo)
    
    2. Console (in ra màn hình)
    3. Cofirm (in ra hộp xác nhận một cái gì đó vd như xác nhận độ tuổi)
    4. Promf (in ra một bản thông báo như alert và sử dụng để hiển thị hộp thoại thông báo nhắc người dùng nhập vào dữ liệu dưới dạng văn bản)
    5. Set Timeout (dùng để chạy một dòng code or funtion trong khoảng thời gian mà mình thiết lập thực thi đúng 1 lần)

    setTimeout(function() {
        alert('hello)
    }, 1000)

    6. Set Interval (như settimeout nhưng thực thi liên tục sau một khoảng thời gian do dev code)
 */

/*
    Toán Tử Trong JS 
    1. Toán tử số học - Arithmetic
    2. Toán tử gán - Assignment 
    3. Toán tử so sánh - Comparision
    4. Toán tử logic - Logical (là toán tử kết nối hai hay nhiều biểu thức, dùng để kiểm tra mối quan hệ logic giữa các biểu thức.)
*/

/*
    Toán Tử số học 
    + --> cộng
    - --> trừ 
    * --> nhân
    / --> chia
    ** --> lũy thừa (mũ :>)
    % --> chia lấy số dư
    ++ --> tăng 1 giá trị số
    -- --> giảm 1 giá trị số   
*/
var a = 2;
var output = a++;
console.log(output);

/*
boolean
 */
/* Kiểu dữ liệu trong JS
1. Kiểu dữ liệu nguyên thủy - Primitive Data
    - Number
    - String
    - Boolean
    - Undefined
    - Null
    - Symbo

    => Kiểu nguyên thủy là kiểu dữ liệu cơ bản nhất có sẵn trong Java 

2. Dữ liệu phức tạp - Complex Data
    - Funtion
    - ObjectS
    => là cấu trúc dữ liệu (data structure) lồng nhau bao gồm các kiểu dữ liệu nguyên thủy (primitive data type)
 */

//number type 
var a = 1;
var b = 2;
var c = 1.5;

// string type
var fullName = "Tiến Đạt"
console.log(fullName)

// boolean
var isSuccess = true;

// Undefined type 
var age;
console.log(age);

//null
var isNull = null; // ko co gi
console.log(typeof isNull);

//symbol
var id = Symbol('id'); //unique(duy nhất)
var id = Symbol('id'); //unique(duy nhất)
console.log(id === id);


// funtion 
// var myFunction = function(){
//     alert('js xin chao the gioi');

// }
// myFunction();

// object type

var myObject = {
    name: 'Tien Dat',
    age: 22, 
    adress: 'Ha Noi',
    // myFunction: function(){

    // }
};

console.log('myObject',myObject);

var myArray = [
    'js',
    'php',
    'java',
    'ruby'
];
console.log(myArray);

/*
    chuỗi (String)
1. Tạo chuỗi
    - các cách tạo chuỗi
        ->  var fullName = "tien dat";
                        or
            var fullName = new String('tien dat');

    - nên dùng cách nào? lý do? 
    - kiểm tra data type

2. Một số case sử dụng backslash (\)
3. Xem độ dài chuỗi
4. Chú ý độ dài khi viết code
5. Tempalate string ES6
*/

var firstname = 'tien';
var lastname = 'dat';

console.log(`toi la: ${firstname} ${lastname} `);