// làm việc với chuỗi
var myString = 'học js tai F8';
//1. Lenght (xem độ dài Str)
console.log(myString.length);

//2. Find Index (timf vị trí của một ký tự trong Str)
console.log(myString.indexOf('amet'));

//3. Cut String (cắt chuỗi)
console.log(myString.slice(-3, -1));

//4. Replace (ghi đè)
console.log(myString.replace('Lorem', 'lr'));
// để ghi đề tất cả thì dùng hệ chính quy replace(/Lorem/g, 'lr')

//5. Conver to upper case 
console.log(myString.toUpperCase());

//6. Conver to lower case 
console.log(myString.toLowerCase());

//7. Trim (dùng để xóa tất cả những khoảng trắng ở đầu và cuối chuỗi)
console.log(myString.trim());

//8. Split ( sử dụng để chia một chuỗi thành một mảng các chuỗi con và trả về mảng mới bằng điểm chung)
var list = 'JS, PHP, RUBY, JAVA, NodeJs'
console.log(list.split(''));

//9. Get a character by index (lấy một ký tự bởi một index cho trước)
var list2 = 'tien dat'
console.log(list2.charAt(3));

//10. template ES6
 var firstname = "tiến"
 var lastname = "đạt"
console.log(`Tên tôi là: ${firstname} ${lastname}`);

